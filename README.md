## PermutasyonApp

This project was generated with C# , Console Application

## System Requirements

C# , .NET Framework version 4.6.* , Visual Studio IDE 

## Solution

The application starts with the user entering 2 texts.
1. Full text
2. Text / letter group to search

* The letters of all text entered and the letters of the searched text are singularized in character type.
* During the singularization process, we also get information about how many times which letter is mentioned in the text.
* Meanwhile, a Dictionary of the letters we are looking for is added
* We navigate through the searched text with a loop and check whether each letter corresponds to the searched letter in the whole text.

* With the result, we determine whether the search text can be written in large text.




