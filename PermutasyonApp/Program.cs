﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermutasyonApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-> Anagram Hesaplama <-");
            Console.Write("Lütfen bir metin giriniz : ");
            string allText = Console.ReadLine();
            Console.Write(string.Format("Lütfen {0} içerisinde aradığınız harfleri/metni giriniz : ", allText));
            string searchText = Console.ReadLine();

            bool result = IsAnagram(searchText.Trim(), allText.Trim());
            Console.WriteLine(string.Format("Sonuç : {0} => {1} = {2} ", allText, searchText, result));
            Console.ReadKey();
        }

        private static Dictionary<char, int> LettersInTextSingular(string input)
        {
            var letters = new Dictionary<char, int>();
            foreach (var key in input)
            {
                if (!letters.ContainsKey(key))
                {
                    letters.Add(key, 0);
                }
                ++letters[key];
            }
            return letters;
        }

        private static bool IsAnagram(string firstText, string secondText)
        {
            var firstTextResult = LettersInTextSingular(firstText);
            var secondTextResult = LettersInTextSingular(secondText);

            foreach (var key in firstTextResult.Keys)
            {
                if (!secondTextResult.ContainsKey(key)) return false;
                if (firstTextResult[key] != secondTextResult[key]) return false;
            }
            return true;
        } 
    }
}
